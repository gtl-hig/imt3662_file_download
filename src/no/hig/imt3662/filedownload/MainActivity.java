package no.hig.imt3662.filedownload;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Simple activity demonstrating the way a file can be downloaded 
 * via HTTP from a web server. It showcases three patterns:
 * - downloading an image
 * - downloading a pure HTML file
 * - downloading RSS feed
 * 
 * 
 * This is part of Mobile Programming course.
 * 
 * Copyright (C) 2013 HiG.
 */
public class MainActivity extends Activity {

	
	final static String IMAGE_URL = "http://gtl-gear.appspot.com/static/img/gtl-logo_h57.png";
	final static String TEXT_URL = "http://google.no";
	final static String RSSFEED_URL = "http://rss.slashdot.org/Slashdot/slashdotGames";
	
    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        try {
			new DownloadImageTask().execute(new URL(IMAGE_URL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
        
        
        
        try {
			new DownloadTextTask().execute(new URL(TEXT_URL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
        
        try {
			new DownloadRSSTask().execute(new URL(RSSFEED_URL));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
       
    }
    
    
    
    
    
    /**
     * Task, responsible for downloading a string from a given URL.
     * This task will execute the download asynchronously, and fill up the 
     * UI field when ready. 
     * 
     * The class is parameterized with the way we want to handle progress,
     * and what we want to return as the result of executing the action. 
     * In our case here, we do not care about progress, and the result 
     * is a String.
     * 
     * Remember, asynchronous task cannot manipulate 
     * UI elements - these need to be updated from within the main thread, 
     * and this is why we put the code into onPostExecute.
     * 
     */
    private class DownloadTextTask extends AsyncTask<URL, Void, String> {
    	@Override
    	protected String doInBackground(URL... urls) {
            assert urls.length == 1; // sanity check
            return downloadText(urls[0]);
        }
    	@Override
        protected void onPostExecute(String result) {
            //If we wanted to do a straight text download we can do it with a similar function
            final TextView txt = (TextView) findViewById(R.id.text);
            if (result.length() > 15) {
            	result = result.substring(0, 15);
            }
            txt.setText(result); 
        }
    }
    
    /**
     * This function downloads the string of text at the URL
     * location passed and then returns content at a Java String
     * @param  URL           a String containing an absolute URL giving the base location and name of the text
     * @return returnString  the String constructed using a buffered reader
     *
     */
    private String downloadText(URL url) {
    	int BUFFER_SIZE = 2000;
        InputStream in = null;
        try {
            in = openHttpConnection(url);
        } catch (IOException e1) {
            e1.printStackTrace();
            return "";
        }
        
        InputStreamReader isr = new InputStreamReader(in);
        int charRead;
        
        String returnString = "";
        char[] inputBuffer = new char[BUFFER_SIZE];          
        try {
            while ((charRead = isr.read(inputBuffer))>0)
            {                    
                //---convert the chars to a String---
                String readString = 
                    String.copyValueOf(inputBuffer, 0, charRead);                    
                returnString += readString;
                inputBuffer = new char[BUFFER_SIZE];
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }    
        return returnString;
    }
    
    
    
    
    /**
     * Task, responsible for downloading an image from a given URL.
     * This task will execute the download asynchronously, and fill up the 
     * UI field when ready.
     * We are not interested in counting progress. We are returning 
     * a Bitmap from the task.
     */
    private class DownloadImageTask extends AsyncTask<URL, Void, Bitmap> {
    	@Override
    	protected Bitmap doInBackground(URL... urls) {
    		assert urls.length == 1; // sanity check
            return downloadImage(urls[0]);
        }
    	@Override
    	protected void onPostExecute(Bitmap bitmap) {
            //Then display the image to a view
            final ImageView img = (ImageView) findViewById(R.id.img);
            img.setImageBitmap(bitmap);
    	}
    }
    
    /**
     * This function downloads the image at the URL
     * location passed and then returns the bitmap
     * @param  URL     an absolute URL giving the base location and name of the image
     * @return bitmap  the image at the specified URL
     *
     */
    private Bitmap downloadImage(final URL url) {
        Bitmap bitmap = null;
        InputStream in = null;        
        try {
            in = openHttpConnection(url);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;                
    }

    
    

   
    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously, and fill up the 
     * UI field when ready.
     */
    private class DownloadRSSTask extends AsyncTask<URL, Void, String[]> {
    	@Override
    	protected String[] doInBackground(URL... urls) {
            assert urls.length == 1;
            return downloadRSS(urls[0]);
        }
    	@Override
    	protected void onPostExecute(String[] results) {
            final ListView itemlist = (ListView) findViewById(R.id.itemlist); 
            final ArrayAdapter<String> adapter = 
            		new ArrayAdapter<String>(MainActivity.this, 
            				android.R.layout.simple_list_item_1, 
            				results); // more needed in the resources
            itemlist.setAdapter(adapter);  // assign the titles to the list
    	}
    }
    
    /**
     * This function downloads and parses an rss feed from a specific
     * location passed in the URL and then
     * @param  URL          a String containing the absolute URL giving the 
     * 						base location and name of the rss feed
     * @return RSSTitles    an Array of Strings which are the titles from the RSS feed
     */ 
    private String[] downloadRSS(final URL url) {
        InputStream in = null;
        String[] RSSTitles = new String[0];
        try {
            in = openHttpConnection(url);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            
            try {
                db = dbf.newDocumentBuilder();
                doc = db.parse(in);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }        
            
            doc.getDocumentElement().normalize(); 
            
            //---retrieve all the <item> nodes---
            NodeList itemNodes = doc.getElementsByTagName("item"); 
            
            String strTitle = "";
            RSSTitles = new String[itemNodes.getLength()];
            for (int i = 0; i < itemNodes.getLength(); i++) { 
                Node itemNode = itemNodes.item(i); 
                if (itemNode.getNodeType() == Node.ELEMENT_NODE) { 
                    //---convert the Node into an Element---
                    Element itemElement = (Element) itemNode;
                    
                    //---get all the <title> element under the <item> 
                    // element---
                    NodeList titleNodes = (itemElement).getElementsByTagName("title");
                    
                    //---convert a Node into an Element---
                    Element titleElement = (Element) titleNodes.item(0);
                    
                    //---get all the child nodes under the <title> element---
                    NodeList textNodes = 
                        ((Node) titleElement).getChildNodes();
                    
                    //---retrieve the text of the <title> element---
                    strTitle = ((Node) textNodes.item(0)).getNodeValue();
                    RSSTitles[i] = strTitle;
                } 
            }
        } catch (IOException e1) {
            e1.printStackTrace();            
        }
        return RSSTitles;
    }
    
    /**
     * Handles some of the complexity of opening up a HTTP connection
     * @param  URL   a String containing the absolute URL giving the base location and name of the content
     * @return in    an inputStream which will be the stream of text from the server
     */    
    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();
                 
        if (!(conn instanceof HttpURLConnection)) {                     
            throw new IOException("Not an HTTP connection");
        }
        
        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect(); 

            response = httpConn.getResponseCode();                 
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();                                 
            }                     
        } catch (Exception ex) {
        	ex.printStackTrace();            
        }
        return in;     
    }
    
}
